# app/urls.py
# from django.conf.urls import url
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from app import views

urlpatterns = [
    #url(r'^$', views.HomePageView.as_view()),
    path('', views.home, name='home'),
    path('view1/', views.view1, name='view1'),
    path('search/', views.search, name='search'),
    path('view/<inc_num>', views.view_mgt, name='view_mgt'),
    path('view1/<inc_num>', views.view, name='view'),
    path('incident/', views.incident, name='incident'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
