import re
import datetime

def check_exact_length(text, length):
    if (not text) or (text and len(text) != length):
        return 1
    return None

def validate_incident_num(text):
    """
    Validate if Incident number is in the following format:
    1)KBN-xxxx-xxxx
    2)xxx-xxxx-xxxx
    where x is one-byte alphanumeric
    """
    rule = re.compile(r'^[a-zA-Z0-9]{3,3}-[a-zA-Z0-9]{4,4}-[a-zA-Z0-9]{4,4}$')
    if not rule.search(text):
        return 1
    return None

def validate_date(date):
    """
    Validate if date is in the following format:
    1) YYYY/MM/DD
    2) YYYY/M/D
    """
    if date is not None and len(date.strip()) == 0:
        return 0

    format_string = "%Y/%m/%d"
    try:
        datetime.datetime.strptime(date, format_string)
        return None
    except (ValueError, Exception) as e:
        return 1
