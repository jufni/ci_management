from functools import partial
from django import forms
from app import constants
from app.constants import Option, CompletionFlag, FlagState, CompletionStatus

FLAGS = [
    (CompletionFlag.未完了.value, CompletionFlag.未完了.name),
    (CompletionFlag.完了.value, CompletionFlag.完了.name),
    (CompletionFlag.すべて.value, CompletionFlag.すべて.name),
]

STATES = [
    (FlagState.ON.name, FlagState.ON.name),
    (FlagState.OFF.name, FlagState.OFF.name),
]

STATES_MENU = [
    (FlagState.ON.name, FlagState.ON.value),
    (FlagState.OFF.name, FlagState.OFF.value),
    (FlagState.ALL.name, FlagState.ALL.value),
]

COMPLETION_STATES = [
    (0, CompletionStatus.INCOMPLETE.value),
    (1, CompletionStatus.COMPLETE.value),
]

class HomeForm(forms.Form):
    date = forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date'}))
    completion_flag = forms.CharField(
        widget=forms.Select(attrs={'style': 'height:30px; width:95px; background-color: white;'},
                            choices=FLAGS))
    flag_state = forms.CharField(
        widget=forms.Select(attrs={'style': 'height:30px; width:95px; background-color: white;'},
                            choices=STATES_MENU))

class IncidentForm(forms.Form):
    inc_num = forms.CharField(label='inc_num', max_length=13)

class IncidentDetailsForm(forms.Form):
    inc_num = forms.CharField(label='inc_num', required=False, max_length=13)  # インシデント番号
    inc_num_hidden = forms.CharField(label='inc_num_hidden', max_length=13,
                                     error_messages={'required': constants.INC_NUM_REQUIRED_ERR})
    user_name = forms.CharField(label='user_name', required=False, max_length=50,
                                error_messages={'max_length': constants.GEN_MAXLENGTH_ERR.format('顧客名', 50)})  # 顧客名
    system_name = forms.CharField(label='system_name', required=False, max_length=50,
                                  error_messages={
                                      'max_length': constants.GEN_MAXLENGTH_ERR.format('システム名', 50)})  # システム名
    occurrence_datetime = forms.CharField(label='occurrence_datetime', required=False, max_length=10,
                                          error_messages={
                                              'max_length': constants.GEN_MAXLENGTH_ERR.format('発生日時', 10)})  # 発生日時
    flag_state = forms.CharField(required=False,
                                 widget=forms.Select(  # フラグ状態
                                     attrs={'style': 'border-style: none; margin: 2px 0px 0px 2px; font-size: 14px; '
                                                     'width: 122px; height: 20px;'},
                                     choices=STATES))
    occurrence_event = forms.CharField(label='occurrence_event', required=False, max_length=4000,
                                       error_messages={
                                           'max_length': constants.GEN_MAXLENGTH_ERR.format('発生事象', 4000)})  # 発生事象
    business_impact = forms.CharField(label='business_impact', required=False, max_length=4000,
                                      error_messages={
                                          'max_length': constants.GEN_MAXLENGTH_ERR.format('業務影響', 4000)})  # 業務影響
    support_situation = forms.CharField(label='support_situation', required=False, max_length=4000,
                                        error_messages={
                                            'max_length': constants.GEN_MAXLENGTH_ERR.format('対応状況', 4000)})  # 対応状況
    next_action = forms.CharField(label='next_action', required=False, max_length=4000,
                                  error_messages={
                                      'max_length': constants.GEN_MAXLENGTH_ERR.format('次アクション', 4000)})  # 次アクション 
    cause = forms.CharField(label='cause', required=False, max_length=4000,
                            error_messages={'max_length': constants.GEN_MAXLENGTH_ERR.format('発生原因', 4000)})  # 発生原因
    provisional_measures = forms.CharField(label='provisional_measures', required=False, max_length=4000,
                                           error_messages={'max_length': constants.GEN_MAXLENGTH_ERR.format('対処（暫定）',
                                                                                                            4000)})  # 対処（暫定）
    cope = forms.CharField(label='cope', required=False, max_length=4000,
                           error_messages={'max_length': constants.GEN_MAXLENGTH_ERR.format('対処（恒久）', 4000)})  # 対処（恒久）
    completion_flag = forms.CharField(required=False,
                                      widget=forms.Select(  # 完了フラグ
                                          attrs={
                                              'style': 'border-style: none; margin: 2px 0px 0px 2px; font-size: 14px; '
                                                       'width: 122px; height: 20px;'},
                                          choices=COMPLETION_STATES))
