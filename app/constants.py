from enum import Enum

INC_NUM_MAXLENGTH = 13
INC_NUM_MAXLENGTH_ERR = 'インシデント番号が13文字であることを確認してください。'
INC_NUM_FORMAT_ERR = '「xxx-xxxx-xxxx」の形式で入力してください。'
INVALID_DATE_ERR = '発生日の形式が正しくありません。「YYYY/MM/DD」で入力してください。'
INVALID_PERIOD_DATE_ERR = '抽出期間の形式が正しくありません。日付を「YYYY/MM/DD」で入力してください。'
INC_NUM_REQUIRED_ERR = 'インシデント番号を入力してください。'
REQUIRED_ERR = '{0}を入力してください。'
DATE_REQUIRED_ERR = '発生日時の形式が正しくありません。「YYYY/MM/DD」で入力してください。'
ADD_SUCCESS_INFO = 'インシデント「{0}」が追加されました。'
UPDATE_SUCCESS_INFO = 'インシデント「{0}」が更新されました。'
NOT_FOUND = 'インシデント「{0}」が見つかりませんでした。'
GEN_MAXLENGTH_ERR = '{0}の値が{1}文字以下であることを確認してください。'

class CompletionFlag(Enum):
    未完了 = 0
    完了 = 1
    すべて = -1

class Option(Enum):
    リアルタイム表示 = 0
    期間表示 = 1

class FlagState(Enum):
    ON = 'ON'
    OFF = 'OFF'
    ALL = 'すべて'

class CompletionStatus(Enum):
    INCOMPLETE = '0： 完了以外'
    COMPLETE = '1： 完了'

