# app/views.py

import pyodbc
import datetime
from django.shortcuts import render
from app import sql_strings
from app import validation
from app import constants
from .forms import IncidentForm, IncidentDetailsForm, HomeForm
from app.constants import CompletionFlag, Option, FlagState
from django.conf import settings

def home(request):
    print("=> Process home page called")
    # print("=====> request.POST => ")
    # print(request.POST)
    home_page = 'app/home.html'
    try:
        context = get_realtime_data_management()
        return render(request, home_page, context)
    except (KeyError, pyodbc.OperationalError, pyodbc.DataError, Exception) as e:
        context = {}
        context['error_message'] = str(e)
        context['form'] = get_realtime_form()
        return render(request, home_page, context)

def view1(request):
    print("=> Process view1 page called")
    # Uncomment to DEBUG ------------------------
    # print("=====> request.POST => ")
    # print(request.POST)
    # -------------------------------------------
    view1_page = 'app/view1.html'
    try:
        if 'search' in request.POST:
            return search(request)
        else:
            form = None
            if 'refresh' in request.POST:
                option = int(request.POST['option_filter'])
                # If REALTIME option
                if option == Option.リアルタイム表示.value:
                    context = get_realtime_data()
                # If Periodic option
                else:
                    from_date = None
                    to_date = None
                    if 'startDate' in request.POST:
                        from_date = request.POST['startDate']
                    if 'endDate' in request.POST:
                        to_date = request.POST['endDate']
                    context = get_periodic_data(from_date, to_date,
                                                int(request.POST['completion_flag']), request.POST['flag_state'])
                    form = HomeForm(initial={'completion_flag': request.POST['completion_flag'],
                                             'flag_state': request.POST['flag_state']})
                    context['option_filter'] = option
                    context['form'] = form
                    context['startDate'] = from_date
                    context['endDate'] = to_date
            else:
                context = get_realtime_data()
            return render(request, view1_page, context)
    except (KeyError, pyodbc.OperationalError, pyodbc.DataError, Exception) as e:
        context = {}
        if '日付' in str(e):
            context['error_message'] = constants.INVALID_PERIOD_DATE_ERR
        else:
            context['error_message'] = str(e)
        context['form'] = get_realtime_form()
        return render(request, view1_page, context)

def view(request, inc_num):
    print("=> Process view() called")
    context = {}
    page = 'app/view.html'
    try:
        if validation.validate_incident_num(inc_num):
            context['inc_num'] = inc_num
            context['error_message'] = constants.INC_NUM_FORMAT_ERR
        else:
            context = read(inc_num)

            if 'not_existing' in context:
                context = {'インシデント番号': inc_num}
                context['error_message'] = constants.NOT_FOUND.format(context['インシデント番号'])

            form = IncidentDetailsForm(initial={'flag_state': context['フラグ状態'],
                                                'completion_flag': context['完了フラグ']})
            context['form'] = form
        return render(request, page, context)
    except (KeyError, TypeError, pyodbc.DataError, pyodbc.OperationalError, Exception) as e:
        context['error_message'] = str(e)
        return render(request, page, context)

def view_mgt(request, inc_num):
    print("=> Process view() called")
    context = {}
    page = 'app/view_mgt.html'
    try:
        if validation.validate_incident_num(inc_num):
            context['inc_num'] = inc_num
            context['error_message'] = constants.INC_NUM_FORMAT_ERR
        else:
            context = read(inc_num)

            if 'not_existing' in context:
                context = {'インシデント番号': inc_num}
                context['error_message'] = constants.NOT_FOUND.format(context['インシデント番号'])

            form = IncidentDetailsForm(initial={'flag_state': context['フラグ状態'],
                                                'completion_flag': context['完了フラグ']})
            context['form'] = form
        return render(request, page, context)
    except (KeyError, TypeError, pyodbc.DataError, pyodbc.OperationalError, Exception) as e:
        context['error_message'] = str(e)
        return render(request, page, context)

def incident(request):
    print("=> Process incident() called")
    # print("=====> request.POST => ")
    # print(request.POST)

    inc_num = None
    context = {}
    page = 'app/details.html'
    updateFlag = 'update' in request.POST
    addFlag = 'add' in request.POST
    try:
        if updateFlag is True or addFlag is True:
            incidentDetailsForm = IncidentDetailsForm(request.POST or None, request.FILES or None)
            inc_num = request.POST['inc_num_hidden']
            context['inc_num'] = inc_num
            if not incidentDetailsForm.is_valid():
                context = read(inc_num)
                context['インシデント番号'] = inc_num
                form = IncidentDetailsForm(initial={'flag_state': request.POST['flag_state'],
                                                    'completion_flag': request.POST['completion_flag']})
                context['form'] = form
                return render(request, page, context)
            else:
                check_date_flag = validation.validate_date(request.POST['occurrence_datetime'])
                if check_date_flag is not None:
                    context = read(inc_num)
                    context['インシデント番号'] = inc_num
                    if check_date_flag == 0:
                        context['error_message'] = constants.REQUIRED_ERR.format('発生日時')
                    elif check_date_flag == 1:
                        context['error_message'] = constants.INVALID_DATE_ERR

                    form = IncidentDetailsForm(initial={'flag_state': request.POST['flag_state'],
                                                        'completion_flag': request.POST['completion_flag']})
                    context['form'] = form
                    return render(request, page, context)

        # If from details page - UPDATE action
        if updateFlag is True:
            update(request.POST)
        # If from details page - ADD action
        elif addFlag is True:
            add(request.POST)
        else:
            # If from search.html or page1
            inc_num = request.POST['inc_num']
            context = check_inc_num(request, context, inc_num)
            if context:
                return context

        context = read(inc_num)
        # If already existing in database
        if 'existing' in context:
            if context['existing'] is True and addFlag is True:
                context['info_message'] = constants.ADD_SUCCESS_INFO.format(context['インシデント番号'])
            elif context['existing'] is True and updateFlag is True:
                context['info_message'] = constants.UPDATE_SUCCESS_INFO.format(context['インシデント番号'])
            form = IncidentDetailsForm(
                initial={'flag_state': context['フラグ状態'].strip(), 'completion_flag': context['完了フラグ']})
            # If NOT existing
        else:
            form = IncidentDetailsForm()

        if not context:
            context = {'インシデント番号': inc_num}

        context['form'] = form
        return render(request, page, context)
    except (KeyError, TypeError, pyodbc.DataError, pyodbc.OperationalError, Exception) as e:
        context['error_message'] = str(e)
        return render(request, get_err_page(updateFlag, addFlag), context)

def get_periodic_data(from_date, to_date, completion_flag, flag_state):
    print("=> Process get_periodic_data() called")
    conn = get_connection()
    cursor = conn.cursor()
    if flag_state == FlagState.ALL.name:
        cursor.execute(sql_strings.get_incidents_by_period_status(from_date, to_date, completion_flag))
    else:
        cursor.execute(sql_strings.get_incidents_by_period_status(from_date, to_date, completion_flag, flag_state))
    columns = [column[0] for column in cursor.description]
    incidents = cursor.fetchall()
    print("=> incidents.size =" + str(len(incidents)))
    incidents = get_dict_data(incidents, columns)
    context = {'incidents': incidents}
    close(conn)
    return context

def close(conn):
    if conn is not None:
        conn.close()

def get_realtime_form():
    form = HomeForm(initial={'flag_state': FlagState.ALL.name})
    disable_attr(form, 'completion_flag')
    disable_attr(form, 'flag_state')
    return form

def get_realtime_data():
    form = get_realtime_form()
    context = init_data(CompletionFlag.未完了.value)
    context['option_filter'] = 0
    context['form'] = form
    return context

def get_realtime_data_management():
    context = init_data_management(CompletionFlag.未完了.value, FlagState.ON.value)
    return context

def disable_attr(form, attr_str):
    form.fields[attr_str].widget.attrs['disabled'] = 'disabled'

def search(request):
    print("=> Process search() called")
    return render(request, 'app/search.html', context=None)

def check_inc_num(request, context, inc_num):
    page = 'app/search.html'
    context['inc_num'] = inc_num
    if validation.check_exact_length(request.POST['inc_num'], constants.INC_NUM_MAXLENGTH):
        context['error_message'] = constants.INC_NUM_MAXLENGTH_ERR
        return render(request, page, context)
    elif validation.validate_incident_num(request.POST['inc_num']):
        context['error_message'] = constants.INC_NUM_FORMAT_ERR
        return render(request, page, context)
    return None

def get_err_page(updateFlag, addFlag):
    return 'app/details.html' if updateFlag or addFlag else 'app/search.html'

# ------ READ data
def read(inc_num):
    print("=> Process read() called")
    conn = get_connection()
    cursor = conn.cursor()
    num = ''
    print("=> inc_num:" + inc_num)
    if inc_num:
        num = inc_num
    cursor.execute(sql_strings.get_incident_by_number(), num)
    columns = [column[0] for column in cursor.description]
    results = []
    row = cursor.fetchone()

    context = {}

    if row:
        results.append(dict(zip(columns, row)))
        context = results[0]
        context['provisional_measures_val'] = '' if not context.get('対処（暫定）') else context.get('対処（暫定）')
        context['cope_val'] = '' if not context.get('対処（恒久）') else context.get('対処（恒久）')
        context['顧客名'] = '' if not context['顧客名'] else context['顧客名']
        context['システム名'] = '' if not context['システム名'] else context['システム名']

        context['発生日時'] = '' if not context['発生日時'] else normalizeDate(context['発生日時'])

        context['フラグ状態'] = '' if not context['フラグ状態'] else context['フラグ状態'].strip()

        context['発生事象'] = '' if not context['発生事象'] else context['発生事象']
        context['業務影響'] = '' if not context['業務影響'] else context['業務影響']
        context['対応状況'] = '' if not context['対応状況'] else context['対応状況']
        context['次アクション'] = '' if not context['次アクション'] else context['次アクション']
        context['発生原因'] = '' if not context['発生原因'] else context['発生原因']
        context['完了フラグ'] = '' if not context['完了フラグ'] else context['完了フラグ']
        context['existing'] = True
    else:
        context['インシデント番号'] = num
        context['not_existing'] = True

    close(conn)
    return context

def init_data_management(completion_flag, flag_state):
    print("=> Process init_data_management() called")
    conn = get_connection()
    cursor = conn.cursor()
    cursor.execute(sql_strings.get_incidents_by_status_flag(), str(completion_flag), flag_state)
    columns = [column[0] for column in cursor.description]
    incidents = cursor.fetchall()
    print("=> incidents.size = " + str(len(incidents)))
    incidents = get_dict_data(incidents, columns)
    context = {'incidents': incidents}

    close(conn)
    return context

def init_data(completion_flag):
    print("=> Process init_data() called")
    conn = get_connection()
    cursor = conn.cursor()
    if completion_flag < 0:
        cursor.execute(sql_strings.get_incidents_all())
    else:
        cursor.execute(sql_strings.get_incidents_by_status(), str(completion_flag))
    columns = [column[0] for column in cursor.description]
    incidents = cursor.fetchall()
    print("=> incidents.size = " + str(len(incidents)))
    incidents = get_dict_data(incidents, columns)
    context = {'incidents': incidents}

    close(conn)
    return context

def get_dict_data(incidents, columns):
    inc_list = []
    for inc in incidents:
        obj = []
        if inc:
            obj.append(dict(zip(columns, inc)))
            context = obj[0]
            context['provisional_measures_val'] = '' if not context.get('対処（暫定）') else context.get('対処（暫定）')
            context['cope_val'] = '' if not context.get('対処（恒久）') else context.get('対処（恒久）')
            context['顧客名'] = '' if not context['顧客名'] else context['顧客名']
            context['システム名'] = '' if not context['システム名'] else context['システム名']
            context['発生日時'] = '' if not context['発生日時'] else normalizeDate(context['発生日時'])
            context['フラグ状態'] = '' if not context['フラグ状態'] else context['フラグ状態']
            context['発生事象'] = '' if not context['発生事象'] else context['発生事象']
            context['業務影響'] = '' if not context['業務影響'] else context['業務影響']
            context['対応状況'] = '' if not context['対応状況'] else context['対応状況']
            context['次アクション'] = '' if not context['次アクション'] else context['次アクション']
            context['発生原因'] = '' if not context['発生原因'] else context['発生原因']
            context['完了フラグ'] = '' if not context['完了フラグ'] else context['完了フラグ']
            inc_list.append(context)
    return inc_list

def normalizeDate(date):
    try:
        newDate = '/'.join(str.zfill(elem, 2) for elem in date.split('-'))
        date_obj = datetime.datetime.strptime(newDate, '%Y/%m/%d').date()
        return date_obj.strftime('%Y/%m/%d')
    except ValueError as ve:
        return date

# ------ UPDATE Data
def update(data):
    print("=> Process update() called")
    conn = get_connection()
    cursor = conn.cursor()
    if data['occurrence_datetime']:
        cursor.execute(sql_strings.update_incident(data['occurrence_datetime']),
                       data['user_name'],
                       data['system_name'],
                       data['occurrence_datetime'],
                       data['flag_state'],
                       data['occurrence_event'],
                       data['business_impact'],
                       data['support_situation'],
                       data['next_action'],
                       data['cause'],
                       data['provisional_measures'],
                       data['cope'],
                       data['completion_flag'],
                       data['inc_num_hidden'])
    else:
        cursor.execute(sql_strings.update_incident(data['occurrence_datetime']),
                       data['user_name'],
                       data['system_name'],
                       data['flag_state'],
                       data['occurrence_event'],
                       data['business_impact'],
                       data['support_situation'],
                       data['next_action'],
                       data['cause'],
                       data['provisional_measures'],
                       data['cope'],
                       data['completion_flag'],
                       data['inc_num_hidden'])
    conn.commit()
    close(conn)

# ------ ADD New Incident Record
def add(data):
    print("=> Process add() called")
    conn = get_connection()
    cursor = conn.cursor()
    if data['occurrence_datetime']:
        cursor.execute(sql_strings.add_incident(data['occurrence_datetime']),
                       data['inc_num_hidden'],
                       data['user_name'],
                       data['system_name'],
                       data['occurrence_datetime'],
                       data['flag_state'],
                       data['occurrence_event'],
                       data['business_impact'],
                       data['support_situation'],
                       data['next_action'],
                       data['cause'],
                       data['provisional_measures'],
                       data['cope'],
                       data['completion_flag'])
    else:
        cursor.execute(sql_strings.add_incident(data['occurrence_datetime']),
                       data['inc_num_hidden'],
                       data['user_name'],
                       data['system_name'],
                       data['flag_state'],
                       data['occurrence_event'],
                       data['business_impact'],
                       data['support_situation'],
                       data['next_action'],
                       data['cause'],
                       data['provisional_measures'],
                       data['cope'],
                       data['completion_flag'])
    conn.commit()
    close(conn)

def get_connection():
    conn = pyodbc.connect('Driver={SQL Server};'
                          f'Server={settings.DB_SERVER};'
                          f'Database={settings.DB_DATABASE};'
                          f'uid={settings.DB_UID};'
                          f'pwd={settings.DB_PWD};'
                          'Trusted_Connection=no;')
    return conn
